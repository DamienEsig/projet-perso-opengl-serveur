#ifndef INFOCONNECTION_H
#define INFOCONNECTION_H

#include <string>
#include <winsock2.h>
#include <chrono>

struct socketAll
{
    SOCKET sockDeco;
    SOCKET sockBalle;
    SOCKET sockPos;
    SOCKET sockSentinelle;
}typedef socketAll;

class InfoConnexion
{
    public:
        InfoConnexion();
        virtual ~InfoConnexion();
        void setPseudo(std::string pseudo);
        void setSock(socketAll* sockAll);
        void setContinuerReception(bool continuerReception);
        std::string getPseudo();
        socketAll* getSock();
        bool getContinuerReception();
    private:
        std::string m_pseudo;
        socketAll* m_sockAll;
        bool m_continuerReception;
};

#endif // INFOCONNECTION_H
