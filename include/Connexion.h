#ifndef DEF_CONNEXION
#define DEF_CONNEXION

#if defined (WIN32)
    #include <winsock2.h>
    typedef int socklen_t;
#elif defined (linux)
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #define INVALID_SOCKET -1
    #define SOCKET_ERROR -1
    #define closesocket(s) close(s)
    typedef int SOCKET;
    typedef struct sockaddr_in SOCKADDR_IN;
    typedef struct sockaddr SOCKADDR;
#endif

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <list>
#include <thread>
#include <winsock2.h>
#include "InfoConnexion.h"
#define PORT 23

struct socketPosition
{
    char pseudo[20];
    float posX, posY, posZ, angle;
}typedef socketPosition;

struct socketBalle
{
    float posX, posY, posZ, oriX, oriY, oriZ;
}typedef socketBalle;

class Connexion
{
    public :
        Connexion();
        ~Connexion();
        void attenteConnexion();
        void reception(InfoConnexion* info);
        void delListSocket(SOCKET sock);
        void startSentinelle(InfoConnexion* info);
        void lastCheck(InfoConnexion* info, std::chrono::system_clock::time_point *lastCheck);
        void startSentinelleRecep(InfoConnexion* info);
        void delListSocketSentinelle(SOCKET sock);
        void deplacementThread(InfoConnexion*);
        void balleThread(InfoConnexion*);
        void decoThread(InfoConnexion*);
    private :
        std::thread attenteConn;
        std::list<InfoConnexion*> m_listeConnecte;
        bool wait;
        SOCKET m_sock;
        int erreur;
};

#endif
