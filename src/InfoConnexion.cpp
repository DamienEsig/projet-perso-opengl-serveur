#include "InfoConnexion.h"

InfoConnexion::InfoConnexion()
{
    //ctor
}

InfoConnexion::~InfoConnexion()
{
    //dtor
}

void InfoConnexion::setPseudo(std::string pseudo)
{
    m_pseudo = pseudo;
}

void InfoConnexion::setSock(socketAll* sockAll)
{
    m_sockAll = sockAll;
}

void InfoConnexion::setContinuerReception(bool continuerReception)
{
    m_continuerReception = continuerReception;
}

std::string InfoConnexion::getPseudo()
{
    return m_pseudo;
}

socketAll* InfoConnexion::getSock()
{
    return m_sockAll;
}

bool InfoConnexion::getContinuerReception()
{
    return m_continuerReception;
}
