#include "Connexion.h"

Connexion::Connexion()
{
    std::thread thread = std::thread(&Connexion::attenteConnexion, this);
    bool terminer = false;
    std::string command;
    while(!terminer)
    {
        getline(std::cin, command);
        if(command.compare("stop") == 0)
        {
            closesocket(m_sock);
            erreur = 100;
            terminer = true;
        }
        else if(command.compare("liste joueur") == 0)
        {
            std::cout << "Liste des joueurs :" << std::endl;
            if(m_listeConnecte.size() != 0)
            {
                std::list<InfoConnexion*>::iterator it;
                for(it = m_listeConnecte.begin(); it != m_listeConnecte.end(); it++)
                    std::cout <<  "Pseudo : " << (*it)->getPseudo() << std::endl;
            }
            else
                std::cout << "Aucun" << std::endl;
        }
        else if(command.find("kick") == 0 && command.size() > 5)
        {
            std::string pseudoKick = command.substr(5).c_str();
            if(m_listeConnecte.size() != 0)
            {
                bool done = false;
                std::list<InfoConnexion*>::iterator it;
                for(it = m_listeConnecte.begin(); it != m_listeConnecte.end(); it++)
                {
                    if(pseudoKick.compare((*it)->getPseudo()) == 0)
                    {
                        done = true;
                        (*it)->setContinuerReception(false);
                        std::cout << (*it)->getPseudo() << " a ete kick !" << std::endl;
                    }
                }
                if(done == false)
                    std::cout << pseudoKick << " n'est pas connecte !" << std::endl;
            }
            else
                std::cout << "Aucun joueur connecte !" << std::endl;
        }
    }
    thread.join();
}

Connexion::~Connexion()
{

}

void Connexion::attenteConnexion()
{
    #if defined (WIN32)
        WSADATA WSAData;
        erreur = WSAStartup(MAKEWORD(2,2), &WSAData);
    #else
        erreur = 0;
    #endif

    SOCKADDR_IN sin;
    int recsize = sizeof(sin);

    SOCKADDR_IN csin;

    int crecsize = sizeof(csin);

    int sock_err;

    if(!erreur)
    {
        m_sock = socket(AF_INET, SOCK_STREAM, 0);

        if(m_sock != INVALID_SOCKET)
        {
            sin.sin_addr.s_addr = htonl(INADDR_ANY);  /* Adresse IP automatique */
            sin.sin_family = AF_INET;                 /* Protocole familial (IP) */
            sin.sin_port = htons(PORT);               /* Listage du port */
            sock_err = bind(m_sock, (SOCKADDR*)&sin, recsize);

            if(sock_err != SOCKET_ERROR)
            {
                sock_err = listen(m_sock, 5);

                if(sock_err != SOCKET_ERROR)
                {
                    while(!erreur)
                    {
                        InfoConnexion* info = new InfoConnexion;
                        socketAll* csock = new socketAll;
                        csock->sockDeco = accept(m_sock, (SOCKADDR*)&csin, &crecsize);
                        csock->sockPos = accept(m_sock, (SOCKADDR*)&csin, &crecsize);
                        csock->sockBalle = accept(m_sock, (SOCKADDR*)&csin, &crecsize);
                        csock->sockSentinelle = accept(m_sock, (SOCKADDR*)&csin, &crecsize);
                        if(!erreur)
                        {
                            info->setSock(csock);
                            char *connected_ip = inet_ntoa(csin.sin_addr);
                            std::cout << "Connexion initialisee avec " << connected_ip << std::endl;
                            std::thread thread = std::thread(&Connexion::reception, this, info);
                            std::thread sentinelleThread = std::thread(&Connexion::startSentinelle, this, info);
                            thread.detach();
                            sentinelleThread.detach();
                        }
                    }
                }
            }
            closesocket(m_sock);
        }

        #if defined (WIN32)
            WSACleanup();
        #endif
    }
}

void Connexion::startSentinelle(InfoConnexion* info)
{
    info->setContinuerReception(true);
    while(info->getContinuerReception())
    {
        send(info->getSock()->sockSentinelle, "1", 2, 0);
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void Connexion::lastCheck(InfoConnexion* info, std::chrono::system_clock::time_point *lastCheck)
{
    char buffer[2];
    while(info->getContinuerReception())
    {
        if(recv(info->getSock()->sockSentinelle, buffer, 2, 0) != SOCKET_ERROR && info->getContinuerReception())
        {
            *lastCheck = std::chrono::system_clock::now();
        }
    }
}

void Connexion::startSentinelleRecep(InfoConnexion* info)
{
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::chrono::system_clock::time_point lastCheck = std::chrono::system_clock::now();
    std::thread t = std::thread(&Connexion::lastCheck, this, info, &lastCheck);
    while(info->getContinuerReception())
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        now = std::chrono::system_clock::now();
        int dur = std::chrono::duration_cast<std::chrono::seconds>(now-lastCheck).count();
        if(dur >= 3)
            info->setContinuerReception(false);
    }
    t.join();
    std::list<InfoConnexion*>::iterator it;
    for(it = m_listeConnecte.begin(); it != m_listeConnecte.end(); it++)
    {
        std::string pseudo = (*it)->getPseudo();
        if(info->getPseudo() != pseudo)
        {
            send((*it)->getSock()->sockDeco, info->getPseudo().c_str(), 20, 0);
        }
    }
}

void Connexion::deplacementThread(InfoConnexion* info)
{
    socketPosition soPos;
    SOCKET sockPosition = info->getSock()->sockPos;
    while(info->getContinuerReception() && recv(sockPosition, (char *)&soPos, sizeof(soPos), 0) != SOCKET_ERROR)
    {
        std::list<InfoConnexion*>::iterator it;
        for(it = m_listeConnecte.begin(); it != m_listeConnecte.end(); it++)
        {
            SOCKET cPos = (*it)->getSock()->sockPos;
            if(sockPosition != cPos)
            {
                send(cPos, (char *)&soPos, sizeof(soPos), 0);
            }
        }
    }
}

void Connexion::balleThread(InfoConnexion* info)
{
    socketBalle soBa;
    SOCKET sockBalle = info->getSock()->sockBalle;
    while(info->getContinuerReception() && recv(sockBalle, (char *)&soBa, sizeof(soBa), 0) != SOCKET_ERROR)
    {
        std::list<InfoConnexion*>::iterator it;
        for(it = m_listeConnecte.begin(); it != m_listeConnecte.end(); it++)
        {
            SOCKET cBalle = (*it)->getSock()->sockBalle;
            if(sockBalle != cBalle)
            {
                send(cBalle, (char *)&soBa, sizeof(soBa), 0);
            }
        }
    }
}

void Connexion::decoThread(InfoConnexion* info)
{
    char buffer[2];
    SOCKET sockDeco = info->getSock()->sockDeco;
    while(info->getContinuerReception() && recv(info->getSock()->sockDeco, buffer, 2, 0) != SOCKET_ERROR)
    {
        std::list<InfoConnexion*>::iterator it;
        for(it = m_listeConnecte.begin(); it != m_listeConnecte.end(); it++)
        {
            SOCKET cDeco = (*it)->getSock()->sockDeco;
            if(sockDeco != cDeco)
            {
                send(cDeco, info->getPseudo().c_str(), 20, 0);
            }
        }
        std::cout<<"Deconnexion de "<<info->getPseudo()<<std::endl;
        delListSocket(info->getSock()->sockPos);
        info->setContinuerReception(false);
    }
}

void Connexion::reception(InfoConnexion* info)
{
    info->setContinuerReception(true);
    socketAll* sockAll = info->getSock();
    SOCKET sockDeco = sockAll->sockDeco;
    std::thread sentinelleRecep = std::thread(&Connexion::startSentinelleRecep, this, info);
    char pseudoTab[10];
    if(recv(sockDeco, pseudoTab, 10, 0) != SOCKET_ERROR)
    {
        info->setPseudo(pseudoTab);
        if(strcmp(info->getPseudo().c_str(), ""))
        {
            m_listeConnecte.push_back(info);
            std::cout<<"Connexion de "<<info->getPseudo()<<std::endl;
        }
        else
            info->setContinuerReception(false);
    }
    else
        printf("erreur");

    std::thread deplacement = std::thread(&Connexion::deplacementThread, this, info);
    std::thread balle = std::thread(&Connexion::balleThread, this, info);
    std::thread deco = std::thread(&Connexion::decoThread, this, info);

    deplacement.join();
    balle.join();
    deco.join();

    delListSocket(info->getSock()->sockPos);
    info->setContinuerReception(false);
    sentinelleRecep.join();
    closesocket(info->getSock()->sockDeco);
    closesocket(info->getSock()->sockBalle);
    closesocket(info->getSock()->sockPos);
    closesocket(info->getSock()->sockSentinelle);
}

void Connexion::delListSocket(SOCKET sock)
{
    if(m_listeConnecte.size() != 0)
    {
        std::list<InfoConnexion*>::iterator it;
        for(it = m_listeConnecte.begin(); it != m_listeConnecte.end(); it++)
        {
            SOCKET csock = (*it)->getSock()->sockPos;
            if(sock == csock)
                it = m_listeConnecte.erase(it);
        }
    }
}

void Connexion::delListSocketSentinelle(SOCKET sock)
{
    if(m_listeConnecte.size() != 0)
    {
        std::list<InfoConnexion*>::iterator it;
        for(it = m_listeConnecte.begin(); it != m_listeConnecte.end(); it++)
        {
            SOCKET csock = (*it)->getSock()->sockSentinelle;
            if(sock == csock)
                it = m_listeConnecte.erase(it);
        }
    }
}
